import org.junit.Assert;
import org.junit.Test;
import ua.kpi.java.TraversalUtils;
import ua.kpi.java.TreeNode;
import ua.kpi.java.impl.*;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class RedBlackTreeTests {
    @Test
    public void Create_CreateOnlyEmptyRoot_shouldReturnRoot() {
        RedBlackTree<Integer, Integer> tree = new RedBlackTree<>();
        assertNull(tree.getRoot().getVal());
    }

    @Test
    public void Put_PutOneNode_shouldReturnRightValue() {
        RedBlackTree<Integer, Integer> tree = new RedBlackTree<>();
        tree.put(1, 5);
        assertEquals((Integer) 5, tree.getRoot().getVal());
    }

    @Test
    public void Put_PutTwoNodes_shouldReturnRightTwoValues() {
        RedBlackTree<Integer, Integer> tree = new RedBlackTree<>();
        tree.put(1, 5);
        tree.put(2, 9);
        assertEquals((Integer) 5, tree.getRoot().getVal());
        assertEquals((Integer) 9, tree.getRoot().getRight().getVal());
    }

    @Test
    public void Get_GetNodeWithValueTwo_shouldReturnRightValue() {
        RedBlackTree<Integer, Integer> tree = new RedBlackTree<>();
        tree.put(1, 2);
        assertEquals((Integer) 2, tree.getRoot().getVal());
    }

    @Test
    public void Contains_ContainsNodeWithValueFive_shouldReturnRightValue() {
        RedBlackTree<Integer, Integer> tree = new RedBlackTree<>();
        tree.put(5, 2);
        assertTrue(tree.contains(5));
    }

    @Test
    public void Remove_RemoveNodeWithValueFive_shouldReturnFalseOnContains() {
        RedBlackTree<Integer, Integer> tree = new RedBlackTree<>();
        tree.put(5, 2);
        tree.remove(5);
        assertFalse(tree.contains(5));
    }
}
