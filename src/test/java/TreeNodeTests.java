import org.junit.Assert;
import org.junit.Test;
import ua.kpi.java.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TreeNodeTests {

    @Test
    public void addChild_addOneItemToTreeNode_shouldHasOneItem() {
        TreeNode<Integer> tree = new TreeNode<>(5);
        tree.addChild(10);
        TreeNode<Integer> subTree = tree.getChild(0);
        List<TreeNode<Integer>> listChildren = tree.getChildren();
        Integer result = subTree.getData();

        Assert.assertEquals(1, listChildren.size());
        Assert.assertEquals((Integer) 10, result);
    }

    @Test
    public void addChild_addRandomNumberOfItems_shouldHasSomeItems() {
        Random random = new Random();
        int count = random.nextInt(20) + 10;
        List<Integer> numbers = new ArrayList<>();
        TreeNode<Integer> root = new TreeNode<>(random.nextInt());
        for (int i = 0; i < count; i++) {
            int temp = random.nextInt();
            root.addChild(temp);
            numbers.add(temp);
        }
        List<TreeNode<Integer>> listChildren = root.getChildren();

        Assert.assertEquals(count, listChildren.size());
        for (int i = 0; i < count; i++) {
            TreeNode<Integer> child = root.getChild(i);
            Assert.assertEquals(numbers.get(i), child.getData());
        }
    }

    @Test
    public void addChildren_addToTreeWithoutChildren_shouldHasItems() {
        Random random = new Random();
        int count = random.nextInt(20) + 10;
        List<Integer> numbers = new ArrayList<>();

        TreeNode<Integer> root = new TreeNode<>(random.nextInt());
        for (int i = 0; i < count; i++) {
            int temp = random.nextInt();
            numbers.add(temp);
        }
        root.addChildren(numbers);
        List<TreeNode<Integer>> listChildren = root.getChildren();
        Assert.assertEquals(count, listChildren.size());
        for (int i = 0; i < count; i++) {
            Assert.assertEquals(numbers.get(i), listChildren.get(i).getData());
        }
    }
}
