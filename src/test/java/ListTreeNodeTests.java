import org.junit.Test;
import ua.kpi.java.TreeNode;
import ua.kpi.java.impl.ListTreeNode;
import ua.kpi.java.pair.Pair;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class ListTreeNodeTests {
    @Test
    public void isEmpty_isEmptyWithNullHead_shouldReturnTrue() {
        ListTreeNode<Integer,Integer> tree = new ListTreeNode<>();
        tree.put(null, null);
        assertTrue(tree.isEmpty());
    }

    @Test
    public void Size_SizeWithNullHead_shouldReturnZero() {
        ListTreeNode<Integer,Integer> tree = new ListTreeNode<>();
        tree.put(null, null);
        assertEquals(0, tree.size());
    }

    @Test
    public void Size_SizeWithNotNullHead_shouldReturnRightCount() {
        Pair<Integer,Integer> pair = new Pair<>(1,2);
        Pair<Integer,Integer> child1 = new Pair<>(2,3);
        Pair<Integer,Integer> child2 = new Pair<>(3,4);

        ListTreeNode<Integer,Integer> tree = new ListTreeNode<>();

        tree.put(pair.getKey(), pair.getValue());
        tree.put(child1.getKey(), child1.getValue());
        tree.put(child2.getKey(), child2.getValue());
        assertEquals(3, tree.size());
    }

    @Test
    public void Contains_ContainsWithNotThreePairs_shouldReturnTrue() {
        Pair<Integer,Integer> pair = new Pair<>(1,2);
        Pair<Integer,Integer> child1 = new Pair<>(2,3);
        Pair<Integer,Integer> child2 = new Pair<>(3,4);

        ListTreeNode<Integer,Integer> tree = new ListTreeNode<>();

        tree.put(pair.getKey(), pair.getValue());
        tree.put(child1.getKey(), child1.getValue());
        tree.put(child2.getKey(), child2.getValue());

        assertTrue(tree.contains(1));
    }

    @Test
    public void Contains_ContainsWithNullHead_shouldReturnFalse() {
        ListTreeNode<Integer,Integer> tree = new ListTreeNode<>();
        assertFalse(tree.contains(1));
        assertFalse(tree.contains(5));
        assertFalse(tree.contains(8));
    }

    @Test
    public void Get_GetWithThreePairsInside_shouldReturnValueOfTwo() {
        Pair<Integer,Integer> pair = new Pair<>(1,2);
        Pair<Integer,Integer> child1 = new Pair<>(2,3);
        Pair<Integer,Integer> child2 = new Pair<>(3,4);

        ListTreeNode<Integer,Integer> tree = new ListTreeNode<>();

        tree.put(pair.getKey(), pair.getValue());
        tree.put(child1.getKey(), child1.getValue());
        tree.put(child2.getKey(), child2.getValue());

        assertEquals((Integer) 2, tree.get(1));
    }

    @Test
    public void Put_PutOnePair_ShouldReturnThisPairValue() {
        Pair<Integer,Integer> pair = new Pair<>(1,2);
        ListTreeNode<Integer,Integer> tree = new ListTreeNode<>();
        tree.put(pair.getKey(), pair.getValue());
        tree.put(2,3);
        assertTrue(tree.contains(2));
    }

    @Test
    public void Remove_RemoveOnePair_ShouldRemoveThisPair() {
        Pair<Integer,Integer> pair = new Pair<>(1,2);
        ListTreeNode<Integer,Integer> tree = new ListTreeNode<>();
        tree.put(pair.getKey(), pair.getValue());
        tree.put(2,3);
        tree.remove(2);
        assertFalse(tree.contains(2));
    }
}
