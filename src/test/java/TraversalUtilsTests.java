import org.junit.Assert;
import org.junit.Test;
import ua.kpi.java.TraversalUtils;
import ua.kpi.java.TreeNode;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TraversalUtilsTests {
    @Test
    public void BreadthFirstTraversal_BreadthFirstTraversalWithTwoSubtrees_shouldReturnInCorrectOrder() {
        TreeNode<Integer> root = new TreeNode<>(3);
        root.addChild(1);
        root.addChild(5);
        List<Integer> exp = new ArrayList<>();
        exp.add(3);
        exp.add(1);
        exp.add(5);
        List<Integer> resList = TraversalUtils.BreadthFirstTraversal(root);
        for (int i = 0; i < resList.size(); i++) {
            assertEquals(resList.get(i), exp.get(i));
        }
    }

    @Test
    public void BreadthFirstTraversal_BreadthFirstTraversalWithOnlyRoot_shouldReturnRoot() {
        TreeNode<Integer> root = new TreeNode<>(3);
        List<Integer> resList = TraversalUtils.BreadthFirstTraversal(root);
        assertEquals(resList.get(0), root.getData());
    }

    @Test
    public void BreadthFirstTraversal_BreadthFirstTraversalWithNullRoot_shouldReturnNull() {
        TreeNode<Integer> root = null;
        List<Integer> resList = TraversalUtils.BreadthFirstTraversal(root);
        Assert.assertEquals(0, resList.size());
    }

    @Test
    public void BreadthFirstTraversal_BreadthFirstTraversalWithTwoDeepSubtrees_shouldReturnInCorrectOrder() {
        TreeNode<Integer> root = new TreeNode<>(3);
        root.addChild(1);
        root.addChild(5);
        root.getChild(0).addChild(10);
        root.getChild(1).addChild(15);
        List<Integer> exp = new ArrayList<>();
        exp.add(3);
        exp.add(1);
        exp.add(5);
        exp.add(10);
        exp.add(15);
        List<Integer> resList = TraversalUtils.BreadthFirstTraversal(root);
        assertEquals(resList.size(), exp.size());
        for (int i = 0; i < resList.size(); i++) {
            assertEquals(resList.get(i), exp.get(i));
        }
    }

    @Test
    public void DepthFirstTraversal_DepthFirstTraversalWithNullRoot_shouldReturnNull() {
        TreeNode<Integer> root = null;
        List<Integer> resList = TraversalUtils.depthFirstTraversal(root);
        Assert.assertEquals(0, resList.size());
    }

    @Test
    public void DepthFirstTraversal_DepthFirstTraversalWithOnlyRoot_shouldReturnRoot() {
        TreeNode<Integer> root = new TreeNode<>(3);
        List<Integer> resList = TraversalUtils.depthFirstTraversal(root);
        assertEquals(resList.get(0), root.getData());
    }

    @Test
    public void DepthFirstTraversal_DepthFirstTraversalWithTwoDeepSubtrees_shouldReturnInCorrectOrder() {
        TreeNode<Integer> root = new TreeNode<>(3);
        root.addChild(1);
        root.addChild(5);
        root.getChild(0).addChild(10);
        root.getChild(0).addChild(15);
        List<Integer> exp = new ArrayList<>();
        exp.add(10);
        exp.add(15);
        exp.add(1);
        exp.add(5);
        exp.add(3);
        List<Integer> resList = TraversalUtils.depthFirstTraversal(root);
        assertEquals(resList.size(), exp.size());
        for (int i = 0; i < resList.size(); i++) {
            assertEquals(resList.get(i), exp.get(i));
        }
    }
}
