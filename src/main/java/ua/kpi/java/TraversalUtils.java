package ua.kpi.java;

import java.util.*;

public class TraversalUtils {
    public static<T> List<T> BreadthFirstTraversal(TreeNode<T> root) {
        if (root == null)
            return new ArrayList<>();

        Queue<TreeNode<T>> queue = new LinkedList<>();
        List<T> result = new ArrayList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode<T> temp = queue.peek();
            temp.getChildren().forEach(queue::add);
            result.add(queue.remove().getData());
        }
        return result;
    }

    public static<T> List<T> depthFirstTraversal(TreeNode<T> root) {
        if (root == null)
            return new ArrayList<>();

        List<T> result = new ArrayList<>();
        depthFirstTraversalHelper(root, result);
        return result;
    }

    private static<T> void depthFirstTraversalHelper(TreeNode<T> current, List<T> list) {
        for (TreeNode<T> child : current.getChildren()) {
            depthFirstTraversalHelper(child, list);
        }
        list.add(current.getData());
    }
}
