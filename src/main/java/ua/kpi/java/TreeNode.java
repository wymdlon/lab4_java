package ua.kpi.java;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TreeNode<T> implements Iterable<TreeNode<T>> {
    private T data;
    private TreeNode<T> parent;
    private List<TreeNode<T>> children;

    public TreeNode(T data) {
        this.data = data;
        children = new ArrayList<>();
    }

    public void addChild(T item) {
        TreeNode<T> children =  new TreeNode<>(item);
        children.parent = this;
        this.children.add(children);
    }

    public void addChildren(List<T> items) {
        items.forEach(this::addChild);
    }

    public List<TreeNode<T>> getChildren() {
        return new ArrayList<>(children);
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public void setData(T data) {
        this.data = data;
    }

    public TreeNode<T> getChild(int index) {
        if (index >= children.size())
            return null;
        return children.get(index);
    }
    public void setChild(int index, TreeNode<T> child) {
        if (index >= children.size())
            return;
        children.set(index, child);
    }

    public void setParent(TreeNode<T> parent) {
        this.parent = parent;
    }

    public T getData() {
        return data;
    }

    @Override
    public Iterator<TreeNode<T>> iterator() {
        throw new NotImplementedException();
    }
}
