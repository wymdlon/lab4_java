package ua.kpi.java;

import ua.kpi.java.impl.Collection;
import ua.kpi.java.impl.ListTreeNode;
import ua.kpi.java.impl.RedBlackTree;
import ua.kpi.java.pair.Pair;

import java.util.List;

public class ListTree<K, V> {
    private static final int M = 8;
    private static final int countOfBucket = 8;
    private Collection<K, V>[] collections;

    public ListTree() {
        this.collections = new Collection[countOfBucket];
        for (int i = 0; i < countOfBucket; i++) {
            collections[i] = new ListTreeNode<>();
        }
    }

    public void put(K key, V value) {
        Collection<K, V> collection = collections[key.hashCode() % countOfBucket];
        if (collection.size() == M) {
            treefy(key.hashCode() % countOfBucket);
        }
        collection.put(key, value);
    }

    public V get(K key) {
        Collection<K, V> collection = collections[key.hashCode() % countOfBucket];
        return collection.get(key);
    }
    public boolean isEmpty() {
        for (int i = 0; i < countOfBucket; i++) {
            if (!collections[i].isEmpty())
                return false;
        }
        return true;
    }
    public V remove(K key) {
        Collection<K, V> collection = collections[key.hashCode() % countOfBucket];
        V res = collection.remove(key);
        if (collection.size() == M)
            listify(key.hashCode() % countOfBucket);

        return res;
    }
    public boolean contains(K key) {
        Collection<K, V> collection = collections[key.hashCode() % countOfBucket];
        return collection.contains(key);
    }
    public long size() {
        int size = 0;
        for (int i = 0; i < countOfBucket; i++) {
            size += collections[i].size();
        }
        return size;
    }

    private void listify(int index) {
        Collection<K, V> collection = collections[index];
        List<Pair<K, V>> pairs = collection.pairs();
        collections[index] = new ListTreeNode<K, V>();
        pairs.forEach(pair -> collection.put(pair.getKey(), pair.getValue()));
    }

    private void treefy(int index) {
        Collection<K, V> collection = collections[index];
        List<Pair<K, V>> pairs = collection.pairs();
        collections[index] = new RedBlackTree<K, V>();
        pairs.forEach(pair -> collection.put(pair.getKey(), pair.getValue()));
    }
}
