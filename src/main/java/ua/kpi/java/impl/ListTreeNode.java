package ua.kpi.java.impl;

import ua.kpi.java.TreeNode;
import ua.kpi.java.pair.Pair;

import java.util.ArrayList;
import java.util.List;

public class ListTreeNode<K, V> implements Collection<K, V> {
    private TreeNode<Pair<K, V>> head;

    @Override
    public void put(K key, V value) {
        Pair<K, V> pair = new Pair<>(key, value);
        if (key == null || value == null)
            return;

        if (head == null) {
            head = new TreeNode<>(pair);
            return;
        }

        if (contains(key))
            return;

        TreeNode<Pair<K, V>> temp = head;
        while (temp.getChild(0) != null) {
            temp = temp.getChild(0);
        }
        temp.addChild(pair);
    }

    @Override
    public V get(K key) {
        TreeNode<Pair<K, V>> temp = head;
        while (temp != null) {
            K k = temp.getData().getKey();
            if (key.equals(k)) {
                return temp.getData().getValue();
            }
            temp = temp.getChild(0);
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public V remove(K key) {
        TreeNode<Pair<K, V>> temp = head;
        if (head == null)
            return null;

        if (head.getData().getKey().equals(key)) {
            head = head.getChild(0);
            return temp.getData().getValue();
        }

        while (temp.getChild(0) != null) {
            K k = temp.getChild(0).getData().getKey();
            if (key.equals(k)) {
                V value = temp.getChild(0).getData().getValue();
                TreeNode<Pair<K, V>> afterDeleted = temp.getChild(0).getChild(0);
                if (afterDeleted != null)
                    afterDeleted.setParent(temp);

                temp.setChild(0, afterDeleted);
                return value;
            }
            temp = temp.getChild(0);
        }
        return null;
    }

    @Override
    public boolean contains(K key) {
        TreeNode<Pair<K, V>> temp = head;
        while (temp != null) {
            K k = temp.getData().getKey();
            if (key.equals(k)) {
                return true;
            }
            temp = temp.getChild(0);
        }
        return false;
    }

    @Override
    public long size() {
        TreeNode<Pair<K, V>> temp = head;
        int count = 0;
        while (temp != null) {
            count++;
            temp = temp.getChild(0);
        }
        return count;
    }

    @Override
    public List<Pair<K, V>> pairs() {
        TreeNode<Pair<K, V>> temp = head;
        List<Pair<K, V>> pairs = new ArrayList<>();
        while (temp != null) {
            Pair<K, V> pair = temp.getChild(0).getData();
            Pair<K, V> newPair = new Pair<K, V>(pair.getKey(), pair.getValue());
            pairs.add(newPair);
            temp = temp.getChild(0);
        }
        return pairs;
    }
}
