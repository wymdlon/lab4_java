package ua.kpi.java.impl;

import ua.kpi.java.pair.Pair;

import java.util.List;

public class RedBlackNode<K, V> {
    RedBlackTree<K, V> tree;

    private boolean red = false;

    private K key;
    private V val;

    private RedBlackNode<K, V> parent;
    private RedBlackNode<K, V> left;
    private RedBlackNode<K, V> right;

    public K getKey() {
        return key;
    }

    public V getVal() {
        return val;
    }

    public RedBlackNode<K, V> getLeft() {
        return left;
    }

    public RedBlackNode<K, V> getRight() {
        return right;
    }

    public RedBlackNode(RedBlackTree<K, V> tree, RedBlackNode<K, V> parent) {
        this.tree = tree;
        this.parent = parent;
    }


    private RedBlackNode<K, V> grandparent() {
        if (parent != null) {
            return parent.parent;
        } else {
            return null;
        }
    }

    private RedBlackNode<K, V> uncle() {
        RedBlackNode<K, V> g = grandparent();
        if (g == null) {
            return null;
        } else if (parent == g.left) {
            return g.right;
        } else {
            return g.left;
        }
    }

    private RedBlackNode<K, V> sibling() {
        if (parent == null) {
            return null;
        } else if (parent.left == this) {
            return parent.right;
        } else {
            return parent.left;
        }
    }

    private void rotateLeft() {
        RedBlackNode<K, V> pivot = right;
        pivot.parent = parent;
        if (tree.getRoot() == this) {
            tree.setRoot(pivot);
        }
        if (parent != null) {
            if (parent.left == this) {
                parent.left = pivot;
            } else {
                parent.right = pivot;
            }
        }

        right = pivot.left;
        if (pivot.left != null) {
            pivot.left.parent = this;
        }

        parent = pivot;
        pivot.left = this;
    }

    private void rotateRight() {
        RedBlackNode<K, V> pivot = left;
        pivot.parent = parent;
        if (tree.getRoot() == this) {
            tree.setRoot(pivot);
        }
        if (parent != null) {
            if (parent.left == this) {
                parent.left = pivot;
            } else {
                parent.right = pivot;
            }
        }

        left = pivot.right;
        if (pivot.right != null) {
            pivot.right.parent = this;
        }

        parent = pivot;
        pivot.right = this;
    }

    public void put(K key, V val) {
        if (this.key == null) {
            init(key, val);
            insertCase1();
            return;
        }

        int cmp = key.hashCode() - this.key.hashCode();
        if (cmp < 0) {
            left.put(key, val);
        } else if (cmp > 0) {
            right.put(key, val);
        } else {
            this.val = val;
        }
    }

    private void insertCase1() {
        if (parent == null) {
            red = false;
        } else {
            insertCase2();
        }
    }

    private void insertCase2() {
        if (!parent.red) {
            return;
        } else {
            insertCase3();
        }
    }

    private void insertCase3() {
        RedBlackNode<K, V> uncle = uncle();
        RedBlackNode<K, V> grandparent = grandparent();
        if (uncle != null && uncle.red) {
            parent.red = false;
            uncle.red = false;
            grandparent.red = true;
            grandparent.insertCase1();
        } else {
            insertCase4();
        }
    }

    private void insertCase4() {
        RedBlackNode<K, V> grandparent = grandparent();
        if (parent.right == this && parent == grandparent.left) {
            parent.rotateLeft();
            left.insertCase5();
        } else if (parent.left == this && parent == grandparent.right) {
            parent.rotateRight();
            right.insertCase5();
        } else {
            insertCase5();
        }
    }

    private void insertCase5() {
        RedBlackNode<K, V> grandparent = grandparent();

        parent.red = false;
        grandparent.red = true;
        if (this == parent.left && parent == grandparent.left) {
            grandparent.rotateRight();
        } else {
            grandparent.rotateLeft();
        }
    }

    public V get(K key) {
        if (this.key == null) {
            return null;
        }

        int cmp = key.hashCode() - this.key.hashCode();

        if (cmp < 0) {
            return left.get(key);
        } else if (cmp > 0) {
            return right.get(key);
        } else {
            return val;
        }
    }

    public boolean contains(K key) {
        if (this.key == null) {
            return false;
        }

        int cmp = key.hashCode() - this.key.hashCode();
        if (cmp < 0) {
            return left.contains(key);
        } else if (cmp > 0) {
            return right.contains(key);
        } else {
            return true;
        }
    }

    public V remove(K key) {
        if (this.key == null) {
            return null;
        }

        int cmp = key.hashCode() - this.key.hashCode();
        if (cmp < 0) {
            return left.remove(key);
        } else if (cmp > 0) {
            return right.remove(key);
        } else {
            V val = this.val;

            RedBlackNode<K, V> replacement;
            if (right.key != null) {
                replacement = right.min();
            } else if (left.key != null) {
                replacement = left.max();
            } else {
                replacement = this;
                this.key = null;
                this.val = null;
            }

            Pair<K, V> pair = replacement.take();
            this.key = pair.getKey();
            this.val = pair.getValue();
            replacement.deleteOneChild();

            return val;
        }
    }

    private void deleteOneChild() {
        RedBlackNode<K, V> child = (right.key == null) ? left : right;
        child.parent = parent;
        if (this == parent.left) {
            parent.left = child;
        } else {
            parent.right = child;
        }
        if (!red) {
            if (child.red) {
                child.red = false;
            } else {
                child.deleteCase1();
            }
        }
    }

    private void deleteCase1() {
        if (parent != null) {
            deleteCase2();
        }
    }

    private void deleteCase2() {
        RedBlackNode<K, V> s = sibling();

        if (s.red) {
            parent.red = true;
            s.red = false;
            if (this == parent.left) {
                parent.rotateLeft();
            } else {
                parent.rotateRight();
            }
        }
        deleteCase3();
    }

    private void deleteCase3() {
        RedBlackNode<K, V> sibling = sibling();

        if (!parent.red && !sibling.red && !sibling.left.red && !sibling.right.red) {
            sibling.red = true;
            parent.deleteCase1();
        } else {
            deleteCase4();
        }
    }

    private void deleteCase4() {
        RedBlackNode<K, V> s = sibling();

        if (parent.red && !s.red && !s.left.red && !s.right.red) {
            s.red = true;
            parent.red = false;
        } else {
            deleteCase5();
        }
    }

    private void deleteCase5() {
        RedBlackNode<K, V> sibling = sibling();

        if (!sibling.red) {
            if (this == parent.left && !sibling.right.red && sibling.left.red) {
                sibling.red = true;
                sibling.left.red = false;
                sibling.rotateRight();
            } else if (this == parent.right && !sibling.left.red && sibling.right.red) {
                sibling.red = true;
                sibling.right.red = false;
                sibling.rotateLeft();
            }
        }

        deleteCase6();
    }

    private void deleteCase6() {
        RedBlackNode<K, V> sibling = sibling();

        sibling.red = parent.red;
        parent.red = false;

        if (this == parent.left) {
            sibling.right.red = false;
            parent.rotateLeft();
        } else {
            sibling.left.red = false;
            parent.rotateRight();
        }
    }

    public long size() {
        if (key == null) {
            return 0;
        } else {
            return left.size() + right.size() + 1;
        }
    }

    private void init(K key, V val) {
        red = true;
        this.key = key;
        this.val = val;
        left = new RedBlackNode<>(tree, this);
        right = new RedBlackNode<>(tree, this);
    }

    public List<V> elements(List<V> list) {
        if (key == null) {
            return list;
        }

        left.elements(list);
        right.elements(list);
        list.add(val);

        return list;
    }

    public List<K> keys(List<K> list) {
        if (key == null) {
            return list;
        }

        list.add(key);
        left.keys(list);
        right.keys(list);

        return list;
    }

    public List<Pair<K, V>> pairs(List<Pair<K, V>> list) {
        if (key == null) {
            return list;
        }

        left.pairs(list);
        right.pairs(list);
        list.add(new Pair(key, val));

        return list;
    }

    private Pair<K, V> take() {
        Pair<K, V> pair = new Pair<K, V>(key, val);
        key = null;
        val = null;
        return pair;
    }

    private RedBlackNode<K, V> max() {
        if (right.key == null) {
            return this;
        } else {
            return right.max();
        }
    }

    private RedBlackNode<K, V> min() {
        if (left.key == null) {
            return this;
        } else {
            return left.min();
        }
    }

    private long depth() {
        if (key == null) {
            return 0;
        }

        return Math.max(left.depth(), right.depth()) + 1;
    }
}
