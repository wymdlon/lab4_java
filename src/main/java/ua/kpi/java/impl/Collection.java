package ua.kpi.java.impl;

import ua.kpi.java.pair.Pair;

import java.util.List;

public interface Collection<K, V> {
    void put(K key, V value);
    V get(K key);
    boolean isEmpty();
    V remove(K key);
    boolean contains(K key);
    long size();

    List<Pair<K, V>> pairs();
}
