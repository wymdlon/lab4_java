package ua.kpi.java.impl;

import ua.kpi.java.pair.Pair;

import java.util.ArrayList;
import java.util.List;

public class RedBlackTree<K, V> implements Collection<K, V> {
    private RedBlackNode<K, V> root;

    public RedBlackTree() {
        this.root = new RedBlackNode<K, V>(this, null);
    }

    public RedBlackNode<K, V> getRoot() {
        return root;
    }

    public void setRoot(RedBlackNode<K, V> root) {
        this.root = root;
    }

    @Override
    public void put(K key, V val) {
        root.put(key, val);
    }

    @Override
    public V get(K key) {
        return root.get(key);
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(K key) {
        return root.contains(key);
    }

    @Override
    public V remove(K key) {
        if (root.getKey().equals(key) && root.getLeft().getKey() == null && root.getRight().getKey() == null) {
            V ret = root.getVal();
            root = new RedBlackNode<>(this, null);
            return ret;
        }

        return root.remove(key);
    }

    @Override
    public long size() {
        return root.size();
    }

    @Override
    public List<Pair<K, V>> pairs() {
        return root.pairs(new ArrayList<>());
    }


}
